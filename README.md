# Requirements
<p>
	Vue Template requires node.js on the local environment to run.
</p>

# Quick Overview
<ul>
	<li>
		Install Node.js and npm [https://nodejs.org/en/download]
	</li>
	<li>
		Download or clone this repository to your development environment [https://github.com/rascojet/vue-template.git]
	</li>
	<li>
		Run npm install to install the project dependencies [npm install].
	</li>
	<li>
		Open command line and change directory to the app folder [cd vue-template].
	</li>
	<li>
		Run the app [npm run serve]
	</li>
	<li>
		Build the app [npm run build]
	</li>
</ul>

# Demo

http://www.rascojet.com/github/vue-template


## License

[MIT](http://opensource.org/licenses/MIT)